import { useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

const CkEditor = () => {
    const [berita, setBerita] = useState("")

  return (
      <div>
    <CKEditor
      editor={ClassicEditor}
      data={berita}     
      onReady={(editor) => {
        // You can store the "editor" and use when it is needed.
        console.log("Editor is ready to use!", editor);
      }}
      onChange={(event, editor) => {
        const data = editor.getData();
        setBerita(data)
        console.log({ event, editor, data });
      }}
      onBlur={(event, editor) => {
        console.log("Blur.", editor);
      }}
      onFocus={(event, editor) => {
        console.log("Focus.", editor);
      }}
    />
    <div dangerouslySetInnerHTML={{ __html: berita }} />
   </div> 
  );
};

export default CkEditor;
